(function(){

"use strict";

angular.module('silviaemauro', [])

.controller('MainCtrl', ['$scope', function ($scope) {

    $scope.images =  [
        { name : "1.JPG"},
        { name : "2.JPG"},
        { name : "3.JPG"},
        { name : "4.JPG"},
        { name : "5.JPG"},
    ];

    
}])


.config(function(){})
.run(function(){})
/*
    setTimeout(function() {
    $('#imagescontainer').mosaicflow({
        itemSelector: '.item',
        minItemWidth: 160,
    });

    }, 1000);
*/
})()